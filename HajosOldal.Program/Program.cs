﻿// <copyright file="Program.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Program
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using HajosOldal.Data;
    using HajosOldal.Logic;

    /// <summary>
    /// Fő program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main.
        /// </summary>
        /// <param name="args">Paraméter.</param>
        public static void Main(string[] args)
        {
            NonCRUDLogic logic = new NonCRUDLogic();
            FelhasznaloLogic feLogic = new FelhasznaloLogic();
            FoglalasLogic foLogic = new FoglalasLogic();
            HajokLogic hLogic = new HajokLogic();
            UtvonalakLogic uLogic = new UtvonalakLogic();
            Menu(logic, feLogic, foLogic, hLogic, uLogic);
        }

        private static void Menu(NonCRUDLogic logic, FelhasznaloLogic feLogic, FoglalasLogic foLogic, HajokLogic hLogic, UtvonalakLogic uLogic)
        {
            Console.WriteLine("MENÜ");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("11 - Felhasználók kilistázása");
            Console.WriteLine("111 - 1 felhasználó kilistázása");
            Console.WriteLine("12 - Felhasználó hozzáadása");
            Console.WriteLine("13 - Email módosítása");
            Console.WriteLine("14 - Felhasználó nevének módosítása");
            Console.WriteLine("15 - Felahsználó jelszó módosítása");
            Console.WriteLine("16 - Felhasználó telefonszámának módosítása");
            Console.WriteLine("17 - Felhasználó szoszámának módosítása");
            Console.WriteLine("18 - Felhasználó törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("21 - Foglalások kilistázása");
            Console.WriteLine("211 - 1 foglalás kilistázása");
            Console.WriteLine("22 - Foglalás hozzáadása");
            Console.WriteLine("23 - Felhasználó módosítása");
            Console.WriteLine("24 - Fizetési státusz módosítása");
            Console.WriteLine("25 - Foglalás törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("31 - Hajok kilistázása");
            Console.WriteLine("311 - 1 hajó kilistázása");
            Console.WriteLine("32 - Hajó hozzáadása");
            Console.WriteLine("33 - Hajó nevének módosítása");
            Console.WriteLine("34 - Hajó típusának módosítása");
            Console.WriteLine("35 - Személyzet módosítása");
            Console.WriteLine("39 - Hajó törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("41 - Útvonalak kilistázása");
            Console.WriteLine("411 - 1 útvonal kilistázása");
            Console.WriteLine("42 - Útvonal hozzáadása");
            Console.WriteLine("43 - Indulás módosítása");
            Console.WriteLine("44 - Érkezés módosítása");
            Console.WriteLine("45 - Honnan módosítása");
            Console.WriteLine("46 - Hova módosítása");
            Console.WriteLine("47 - Össezs hely módosítása");
            Console.WriteLine("48 - Szabad helyek módosítás");
            Console.WriteLine("49 - Útvonal törlés");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("51 - Hirlevel lista.");
            Console.WriteLine("52 - Fizetett és nem fizetett foglalások aránya.");
            Console.WriteLine("53 - New Yorka tartó utakon a szabad helyek.");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("90 - Java szerver");
            Console.WriteLine("MENÜ VÉGE");

            MenuKezeles(logic, feLogic, foLogic, hLogic, uLogic);
        }

        private static void MenuKezeles(NonCRUDLogic logic, FelhasznaloLogic feLogic, FoglalasLogic foLogic, HajokLogic hLogic, UtvonalakLogic uLogic)
        {
            string selected = Console.ReadLine();
            while (selected != "exit")
            {
                if (selected == "11")
                {
                    Console.WriteLine("Felhasználók listázása");
                    FelhasznaloListazas(feLogic);
                    Console.WriteLine("Felhasználók listázásank vége");
                }
                else if (selected == "111")
                {
                    Console.WriteLine("Írjon be egy id-t");
                    string id = Console.ReadLine();
                    EgyFelhasznalo(feLogic, int.Parse(id));
                }
                else if (selected == "12")
                {
                    Console.WriteLine("Kérem az új felhasznalo adatait");
                    Console.WriteLine("ID;nev");
                    string beadatok = Console.ReadLine();
                    string[] darabok = beadatok.Split(';');
                    Felhasznalo ujFelhasznao = new Felhasznalo()
                    {
                        FelhasznaloID = int.Parse(darabok[0]),
                        Nev = darabok[1],
                    };
                    FelhasznaloHozzadas(feLogic, ujFelhasznao);
                }
                else if (selected == "13")
                {
                    Console.WriteLine("Írja be az új email címet és az ID-t.");
                    Console.WriteLine("ID;EMAIL");
                    string beadatok = Console.ReadLine();
                    string[] darabok = beadatok.Split(';');
                    FelhasznaloEmailModositas(feLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "14")
                {
                    Console.WriteLine("Írja be az új nevet és az id-t");
                    Console.WriteLine("ID;Név");
                    string beadatok = Console.ReadLine();
                    string[] darabok = beadatok.Split(';');
                    FelhasznaloNevModositas(feLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "15")
                {
                    Console.WriteLine("Írja be az új nevet és az id-t");
                    Console.WriteLine("ID;Név");
                    string beadatok = Console.ReadLine();
                    string[] darabok = beadatok.Split(';');
                    FelhasznaloJelszoModositas(feLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "16")
                {
                    Console.WriteLine("Írja be a id-t és az új telfonszámot.");
                    Console.WriteLine("ID;Szám");
                    string[] darabok = Console.ReadLine().Split(';');
                    FelhasznaloTelefonszamModositas(feLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "17")
                {
                    Console.WriteLine("Írja be a id-t és az új szoszámot.");
                    Console.WriteLine("ID;szoszám");
                    string[] darabok = Console.ReadLine().Split(';');
                    FelhasznaloSzoSzamanakModositasa(feLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "18")
                {
                    Console.WriteLine("Írja be az id-t.");
                    string beadat = Console.ReadLine();
                    FelhasznaloTorles(feLogic, int.Parse(beadat));
                }
                else if (selected == "21")
                {
                    FoglalasListazas(foLogic);
                }
                else if (selected == "211")
                {
                    Console.WriteLine("Írja be az ID-t");
                    int id = int.Parse(Console.ReadLine());
                    EgyFoglalas(foLogic, id);
                }
                else if (selected == "22")
                {
                    Console.WriteLine("Írja be az új foglalás adatait");
                    Console.WriteLine("foglalasid;felhaszid;tvonalid");
                    string[] darabok = Console.ReadLine().Split(';');
                    Foglalas ujFogalas = new Foglalas()
                    {
                        FoglalasID = int.Parse(darabok[0]),
                        FelhasznaloID = int.Parse(darabok[1]),
                        UtvonalID = int.Parse(darabok[2]),
                    };
                }
                else if (selected == "23")
                {
                    Console.WriteLine("Írja be az id-t és az új felhasználó id-t");
                    string[] darabok = Console.ReadLine().Split(';');
                    FoglalasFelhModositas(foLogic, int.Parse(darabok[0]), int.Parse(darabok[1]));
                }
                else if (selected == "24")
                {
                    Console.WriteLine("Írja be az id-t és a státuszt");
                    string[] darabok = Console.ReadLine().Split(';');
                    FoglalasFizStatModositas(foLogic, int.Parse(darabok[0]), short.Parse(darabok[1]));
                }
                else if (selected == "25")
                {
                    Console.WriteLine("Írja be az id-t");
                    int id = int.Parse(Console.ReadLine());
                    FoglalasTorles(foLogic, id);
                }
                else if (selected == "31")
                {
                    HajokListazas(hLogic);
                }
                else if (selected == "311")
                {
                    Console.WriteLine("Írjon be egy id-t");
                    EgyHajo(hLogic, int.Parse(Console.ReadLine()));
                }
                else if (selected == "32")
                {
                    Console.WriteLine("Adja meg a hajó adataid");
                    Console.WriteLine("id;nev;tipus");
                    string[] darabok = Console.ReadLine().Split(';');
                    Hajok ujHajo = new Hajok()
                    {
                        HajoID = int.Parse(darabok[0]),
                        Nev = darabok[1],
                        Tipus = darabok[2],
                    };
                    HajoHozzadas(hLogic, ujHajo);
                }
                else if (selected == "33")
                {
                    Console.WriteLine("Írja be az id-t és az új nevet");
                    Console.WriteLine("id;nev");
                    string[] darabok = Console.ReadLine().Split(';');
                    HajoNevModositas(hLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "34")
                {
                    Console.WriteLine("Írja be az id-t és az új típust");
                    Console.WriteLine("id;tipus");
                    string[] darabok = Console.ReadLine().Split(';');
                    HajokTipusModositas(hLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "35")
                {
                    Console.WriteLine("Írja be az id-t és az új személyezetet");
                    Console.WriteLine("id;szemelyzet");
                    string[] darabok = Console.ReadLine().Split(';');
                    HajoSzemModositas(hLogic, int.Parse(darabok[0]), int.Parse(darabok[1]));
                }
                else if (selected == "36")
                {
                    Console.WriteLine("Írjon be egy id-t");
                    int id = int.Parse(Console.ReadLine());
                    HajoTorles(hLogic, id);
                }
                else if (selected == "41")
                {
                    UtvonalakListazas(uLogic);
                }
                else if (selected == "411")
                {
                    Console.WriteLine("Adjon egy egy id-t");
                    EgyUtvonal(uLogic, int.Parse(Console.ReadLine()));
                }
                else if (selected == "42")
                {
                    Console.WriteLine("Adjon meg egy új útvoanlat.");
                    Console.WriteLine("id;honnan;hova");
                    string[] darabok = Console.ReadLine().Split(';');
                    Utvonalak utvonalak = new Utvonalak()
                    {
                        UtvonalID = int.Parse(darabok[0]),
                        Honnan = darabok[1],
                        Hova = darabok[2],
                    };
                    UtvonalHozzaadas(uLogic, utvonalak);
                }
                else if (selected == "43")
                {
                    Console.WriteLine("Adjon meg egy id-t és egy indulási dátumot");
                    Console.WriteLine("id;yyyy.mm.dd");
                    string[] darabok = Console.ReadLine().Split(';');
                    Utvonalak utvonalak = new Utvonalak()
                    {
                        UtvonalID = int.Parse(darabok[0]),
                        Indulas = DateTime.Today,
                    };
                }
                else if (selected == "44")
                {
                    Console.WriteLine("Adjon meg egy id-t és egy érkezést");
                    Console.WriteLine("id;YYYY.MM.DD");
                    string[] darabok = Console.ReadLine().Split(';');
                    UtvonalErkezes(uLogic, int.Parse(darabok[0]), DateTime.Today);
                }
                else if (selected == "45")
                {
                    Console.WriteLine("Adjon meg egy id-t és az indulái pontot.");
                    Console.WriteLine("id;honnan");
                    string[] darabok = Console.ReadLine().Split(';');
                    UtvonalakHonnanModositas(uLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "46")
                {
                    Console.WriteLine("Adjon meg egy id-t és az úticélt.");
                    Console.WriteLine("id;hova");
                    string[] darabok = Console.ReadLine().Split(';');
                    UtvonalkHovaModositas(uLogic, int.Parse(darabok[0]), darabok[1]);
                }
                else if (selected == "47")
                {
                    Console.WriteLine("Adjon megy egy id-t és az új összes helye");
                    Console.WriteLine("id;hely");
                    string[] darabok = Console.ReadLine().Split(';');
                    UtvonalakOsszesHelyMod(uLogic, int.Parse(darabok[0]), int.Parse(darabok[1]));
                }
                else if (selected == "48")
                {
                    Console.WriteLine("Adjon meg egy id-t és az új szabad helyeket");
                    Console.WriteLine("id;hely");
                    string[] darabok = Console.ReadLine().Split(';');
                    UtvonalakSzabadHelyMod(uLogic, int.Parse(darabok[0]), int.Parse(darabok[1]));
                }
                else if (selected == "49")
                {
                    Console.WriteLine("Adjon meg egy id-t.");
                    int id = int.Parse(Console.ReadLine());
                    UtvonalakTorles(uLogic, id);
                }
                else if (selected == "51")
                {
                    var hirlevelLista = logic.Hirlevel();
                    foreach (var egyed in hirlevelLista)
                    {
                        Console.WriteLine("Név: " + egyed.Nev);
                    }
                }
                else if (selected == "52")
                {
                    var fizetettNemFizetettLista = logic.FizetettNemFizetettArany();
                    foreach (var egyed in fizetettNemFizetettLista)
                    {
                        Console.WriteLine(egyed.UtvonalId + " " + egyed.Arany);
                    }
                }
                else if (selected == "53")
                {
                    var toNewYorkHelyek = logic.ToNewYorkHelyek();
                    foreach (var egyed in toNewYorkHelyek)
                    {
                        Console.WriteLine(egyed.UtvonalId + " " + egyed.OsszesHely);
                    }
                }
                else if (selected == "90")
                {
                    Console.WriteLine("Adjon meg egy nevet.");
                    string nev = Console.ReadLine();
                    XDocument xDoc = XDocument.Load("http://localhost:8080/FelevesWeb/Web?nev=" + nev);
                    Console.WriteLine("A generált random nyereményjáték kód:");
                    Console.WriteLine(xDoc.ToString());
                }

                selected = Console.ReadLine();
            }
        }

        // Felhasznalo metódusok
        private static void FelhasznaloListazas(FelhasznaloLogic fLogic)
        {
            var lista = fLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.Nev);
            }
        }

        private static void EgyFelhasznalo(FelhasznaloLogic fLogic, int id)
        {
            Console.WriteLine(fLogic.GetOne(id));
        }

        private static void FelhasznaloHozzadas(FelhasznaloLogic fLogic, Felhasznalo ujFelhasznalo)
        {
            fLogic.Hozzaadas(ujFelhasznalo);
            Console.WriteLine("Felhasználó hozzáadása sikeresen megtörtént.");
        }

        private static void FelhasznaloNevModositas(FelhasznaloLogic fLogic, int id, string uj_nev)
        {
            fLogic.Nev_Modositas(id, uj_nev);
            Console.WriteLine("A felhasználó nevének módosítása megtörtént.");
        }

        private static void FelhasznaloEmailModositas(FelhasznaloLogic fLogic, int id, string ujemail)
        {
            fLogic.Email_Modositas(id, ujemail);
            Console.WriteLine("Felhasználó email címe megváltoztatva.");
        }

        private static void FelhasznaloJelszoModositas(FelhasznaloLogic fLogic, int id, string ujJelszo)
        {
            fLogic.Jelszo_Modositas(id, ujJelszo);
            Console.WriteLine("Felhasználó jelszavának módostása.");
        }

        private static void FelhasznaloTelefonszamModositas(FelhasznaloLogic fLogic, int id, string ujTelefon)
        {
            fLogic.Telefon_Modositas(id, ujTelefon);
            Console.WriteLine("Felhasználü jelszavának módosítása megtörtént");
        }

        private static void FelhasznaloSzoSzamanakModositasa(FelhasznaloLogic fLogic, int id, string ujSzoszam)
        {
            fLogic.SzoSzama_Modositas(id, ujSzoszam);
            Console.WriteLine("Felhasználó szoszámának módosítása megtörtént.");
        }

        private static void FelhasznaloTorles(FelhasznaloLogic fLogic, int id)
        {
            fLogic.Torles(id);
            Console.WriteLine("Felhasználó törölve.");
        }

        // FOGLALAS metódusok
        private static void FoglalasListazas(FoglalasLogic fLogic)
        {
            var lista = fLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void EgyFoglalas(FoglalasLogic foLogic, int id)
        {
            Console.WriteLine(foLogic.GetOne(id));
        }

        private static void FoglalasHozzadas(FoglalasLogic foLogic, Foglalas foglalas)
        {
            foLogic.Hozzaadas(foglalas);
        }

        private static void FoglalasFelhModositas(FoglalasLogic foLogic, int id, int fId)
        {
            foLogic.Felhasznalo_Modositas(id, fId);
        }

        private static void FoglalasFizStatModositas(FoglalasLogic foLogic, int id, short fizstat)
        {
            foLogic.FizStat_Modositas(id, fizstat);
        }

        private static void FoglalasTorles(FoglalasLogic foLogic, int id)
        {
            foLogic.Torles(id);
        }

        // HAJOK metódusok
        private static void HajokListazas(HajokLogic hLogic)
        {
            var lista = hLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void EgyHajo(HajokLogic hLogic, int id)
        {
            Console.WriteLine(hLogic.GetOne(id));
        }

        private static void HajoHozzadas(HajokLogic hLogic, Hajok hajok)
        {
            hLogic.Hozzaadas(hajok);
        }

        private static void HajoNevModositas(HajokLogic hLogic, int id, string nev)
        {
            hLogic.Nev_Modositas(id, nev);
        }

        private static void HajoSzemModositas(HajokLogic hLogic, int id, int szam)
        {
            hLogic.Szemelyzet_Modositas(id, szam);
        }

        private static void HajokTipusModositas(HajokLogic hLogic, int id, string tipus)
        {
            hLogic.Tipus_Modositas(id, tipus);
        }

        private static void HajoTorles(HajokLogic hLogic, int id)
        {
            hLogic.Torles(id);
        }

        // ÚTVONALAK metódusok
        private static void UtvonalakListazas(UtvonalakLogic uLogic)
        {
            var lista = uLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void EgyUtvonal(UtvonalakLogic uLogic, int id)
        {
            Console.WriteLine(uLogic.GetOne(id));
        }

        private static void UtvonalHozzaadas(UtvonalakLogic uLogic, Utvonalak utvonalak)
        {
            uLogic.Hozzaadas(utvonalak);
        }

        private static void UtvonalErkezes(UtvonalakLogic uLogic, int id, DateTime erkezes)
        {
            uLogic.Erkezes_Modositas(id, erkezes);
        }

        private static void UtvonalIndulas(UtvonalakLogic uLogic, int id, DateTime indulas)
        {
            uLogic.Indula_Modositas(id, indulas);
        }

        private static void UtvonalakHonnanModositas(UtvonalakLogic uLogic, int id, string honnan)
        {
            uLogic.Honnan_Modositas(id, honnan);
        }

        private static void UtvonalkHovaModositas(UtvonalakLogic uLogic, int id, string hova)
        {
            uLogic.Hova_Modositas(id, hova);
        }

        private static void UtvonalakOsszesHelyMod(UtvonalakLogic uLogic, int id, int hely)
        {
            uLogic.OsszesHely_Modositas(id, hely);
        }

        private static void UtvonalakSzabadHelyMod(UtvonalakLogic uLogic, int id, int hely)
        {
            uLogic.SzabadHely_Modositas(id, hely);
        }

        private static void UtvonalakTorles(UtvonalakLogic uLogic, int id)
        {
            uLogic.Torles(id);
        }
    }
}
