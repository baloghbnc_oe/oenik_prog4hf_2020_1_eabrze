﻿// <copyright file="FelhasznaloLogic.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// Felhasznalo Logic osztály.
    /// </summary>
    public class FelhasznaloLogic : IFelhasznaloLogic
    {
        private readonly IFelhasznaloRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FelhasznaloLogic"/> class.
        /// </summary>
        /// <param name="repo">Repo.</param>
        public FelhasznaloLogic(IFelhasznaloRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FelhasznaloLogic"/> class.
        /// </summary>
        public FelhasznaloLogic()
        {
            this.repo = new FelhasznaloRepository(new DatabaseEntities());
        }

        /// <summary>
        /// Email módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_email">Új email.</param>
        public void Email_Modositas(int id, string uj_email)
        {
            this.repo.Email_Modositas(id, uj_email);
        }

        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy példány.</returns>
        public Felhasznalo GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="felhasznalo">Új felhasználó.</param>
        public void Hozzaadas(Felhasznalo felhasznalo)
        {
            this.repo.Hozzaadas(felhasznalo);
        }

        /// <summary>
        /// Jelszó módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_jelszo">Új jelszó.</param>
        public void Jelszo_Modositas(int id, string uj_jelszo)
        {
            this.repo.Jelszo_Modositas(id, uj_jelszo);
        }

        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>Felhasználó lista.</returns>
        public IQueryable<Felhasznalo> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(int id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// Személyi okány szám módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szam">Új szám.</param>
        public void SzoSzama_Modositas(int id, string uj_szam)
        {
            this.repo.SzoSzama_Modositas(id, uj_szam);
        }

        /// <summary>
        /// Telefonszám módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_telefon">Új telefon.</param>
        public void Telefon_Modositas(int id, string uj_telefon)
        {
            this.repo.Telefon_Modositas(id, uj_telefon);
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            this.repo.Torles(id);
        }
    }
}
