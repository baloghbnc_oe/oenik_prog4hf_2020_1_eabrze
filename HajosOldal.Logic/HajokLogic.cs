﻿// <copyright file="HajokLogic.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// Hajok logic osztály.
    /// </summary>
    public class HajokLogic : IHajokLogic
    {
        private readonly IHajokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="HajokLogic"/> class.
        /// </summary>
        /// <param name="repo">HajokRepo.</param>
        public HajokLogic(IHajokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HajokLogic"/> class.
        /// </summary>
        public HajokLogic()
        {
            this.repo = new HajokRepository(new DatabaseEntities());
        }

        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        public Hajok GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Hozzáadás.
        /// </summary>
        /// <param name="hajok">Egy hajó.</param>
        public void Hozzaadas(Hajok hajok)
        {
            this.repo.Hozzaadas(hajok);
        }

        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>Hajok lista.</returns>
        public IQueryable<Hajok> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(int id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// Személytet módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szemelyzet">Új személyzet.</param>
        public void Szemelyzet_Modositas(int id, int uj_szemelyzet)
        {
            this.repo.Szemelyzet_Modositas(id, uj_szemelyzet);
        }

        /// <summary>
        /// Típus módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_tipus">Új típus.</param>
        public void Tipus_Modositas(int id, string uj_tipus)
        {
            this.repo.Tipus_Modositas(id, uj_tipus);
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            this.repo.Torles(id);
        }
    }
}
