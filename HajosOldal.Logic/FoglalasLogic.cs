﻿// <copyright file="FoglalasLogic.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// Foglalások logic osztály.
    /// </summary>
    public class FoglalasLogic : IFoglalasLogic
    {
        private readonly IFoglalasRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FoglalasLogic"/> class.
        /// </summary>
        /// <param name="repo">Egy repo.</param>
        public FoglalasLogic(IFoglalasRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoglalasLogic"/> class.
        /// </summary>
        public FoglalasLogic()
        {
            this.repo = new FoglalasRepository(new DatabaseEntities());
        }

        /// <summary>
        /// Felhasználó módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_felhasznaloid">Új felhasználó id.</param>
        public void Felhasznalo_Modositas(int id, int uj_felhasznaloid)
        {
            this.repo.Felhasznalo_Modositas(id, uj_felhasznaloid);
        }

        /// <summary>
        /// Fizetési státusz módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_stat">Új státusz.</param>
        public void FizStat_Modositas(int id, short uj_stat)
        {
            this.repo.FizStat_Modositas(id, uj_stat);
        }

        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        public Foglalas GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Hozzáadás.
        /// </summary>
        /// <param name="foglalas">Új foglalás.</param>
        public void Hozzaadas(Foglalas foglalas)
        {
            this.repo.Hozzaadas(foglalas);
        }

        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>Foglalás lista.</returns>
        public IQueryable<Foglalas> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            this.repo.Torles(id);
        }
    }
}
