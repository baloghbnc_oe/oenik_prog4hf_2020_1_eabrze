﻿// <copyright file="ILogic.cs" company="Balogh bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Ilogic interface.
    /// </summary>
    /// <typeparam name="T">Valamilyen entity.</typeparam>
    public interface ILogic<T>
    {
        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>T típusú lista.</returns>
        IQueryable<T> Listazas();
    }

    /// <summary>
    /// IFelhasználüLogic interface.
    /// </summary>
    public interface IFelhasznaloLogic : ILogic<Felhasznalo>
    {
        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        Felhasznalo GetOne(int id);

        /// <summary>
        /// Hozzáadás.
        /// </summary>
        /// <param name="felhasznalo">Egy felhasználó.</param>
        void Hozzaadas(Felhasznalo felhasznalo);

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(int id, string uj_nev);

        /// <summary>
        /// Jelszó modosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_jelszo">Új jelszó.</param>
        void Jelszo_Modositas(int id, string uj_jelszo);

        /// <summary>
        /// Email módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_email">Új email.</param>
        void Email_Modositas(int id, string uj_email);

        /// <summary>
        /// Telefon módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_telefon">Új telefon.</param>
        void Telefon_Modositas(int id, string uj_telefon);

        /// <summary>
        /// Személyi igazolvány számának módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szam">Új szám.</param>
        void SzoSzama_Modositas(int id, string uj_szam);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IFoglalásLogic interface.
    /// </summary>
    public interface IFoglalasLogic : ILogic<Foglalas>
    {
        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <returns>Egy adott példány.</returns>
        /// <param name="id">Egyedi azonosító.<param>
        Foglalas GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="foglalas">Új foglalás.</param>
        void Hozzaadas(Foglalas foglalas);

        /// <summary>
        /// Felhasználó módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_felhasznaloid">Új felhasználóid.</param>
        void Felhasznalo_Modositas(int id, int uj_felhasznaloid);

        /// <summary>
        /// Fizetési státusz módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_stat">Új stat.</param>
        void FizStat_Modositas(int id, short uj_stat);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IHajokLogic interface.
    /// </summary>
    public interface IHajokLogic : ILogic<Hajok>
    {
        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy példányt ad vissza.</returns>
        Hajok GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="hajok">Új hajó.</param>
        void Hozzaadas(Hajok hajok);

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(int id, string uj_nev);

        /// <summary>
        /// Típus módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_tipus">Új típus.</param>
        void Tipus_Modositas(int id, string uj_tipus);

        /// <summary>
        /// Személyzet módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szemelyzet">Új személyzet.</param>
        void Szemelyzet_Modositas(int id, int uj_szemelyzet);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IUtvonalLogic interface.
    /// </summary>
    public interface IUtvonalakLogic : ILogic<Utvonalak>
    {
        /// <summary>
        /// Egy példány lekérés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy példány.</returns>
        Utvonalak GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="utvonalak">Új útvonal.</param>
        void Hozzaadas(Utvonalak utvonalak);

        /// <summary>
        /// Szabadhely módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szabadHely">Új szabadhely.</param>
        void SzabadHely_Modositas(int id, int uj_szabadHely);

        /// <summary>
        /// Összeshely módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_osszesHely">Új összeshely.</param>
        void OsszesHely_Modositas(int id, int uj_osszesHely);

        /// <summary>
        /// Indulás módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_indulas">Új indulás.</param>
        void Indula_Modositas(int id, DateTime uj_indulas);

        /// <summary>
        /// Érkezés módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_erkezes">Új érekzés.</param>
        void Erkezes_Modositas(int id, DateTime uj_erkezes);

        /// <summary>
        /// Honnan módostás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_honnan">Új honnan.</param>
        void Honnan_Modositas(int id, string uj_honnan);

        /// <summary>
        /// Hova módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_hova">Új hova.</param>
        void Hova_Modositas(int id, string uj_hova);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// A non crud logic interface.
    /// </summary>
    public interface InonCRUDLOGIC
    {
        /// <summary>
        /// Fizetett nem fizetett arány.
        /// </summary>
        /// <returns>FizetettNemFizetettAranay lista.</returns>
        List<FizetettNemFizetettArany> FizetettNemFizetettArany();

        /// <summary>
        /// Hirlevél.
        /// </summary>
        /// <returns>Hirlevel lista.</returns>
        List<Hirlevel> Hirlevel();

        /// <summary>
        /// ToNewYorkHelyek.
        /// </summary>
        /// <returns>ToNewYorkHelyek lista.</returns>
        List<ToNewYorkHelyek> ToNewYorkHelyek();
    }
}
