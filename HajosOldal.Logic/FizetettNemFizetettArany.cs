﻿// <copyright file="nonCRUDLogic.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// FizetettNemFizetett osztály.
    /// </summary>
    public class FizetettNemFizetettArany
    {
        /// <summary>
        /// Gets or sets utvonalid.
        /// </summary>
        public int UtvonalId { get; set; }

        /// <summary>
        /// Gets or sets arany.
        /// </summary>
        public double Arany { get; set; }

        /// <summary>
        /// Equals felülírás.
        /// </summary>
        /// <param name="obj">Egy objektum.</param>
        /// <returns>Igaz vagy hamis.</returns>
        public override bool Equals(object obj)
        {
            if (obj is FizetettNemFizetettArany)
            {
                FizetettNemFizetettArany other = obj as FizetettNemFizetettArany;
                return this.UtvonalId == other.UtvonalId;
            }

            return false;
        }

        /// <summary>
        /// HashCode generálás.
        /// </summary>
        /// <returns>Hash kód.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
