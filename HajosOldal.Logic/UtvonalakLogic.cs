﻿namespace HajosOldal.Logic
{
    using System;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// UtvonalakLogic osztály.
    /// </summary>
    public class UtvonalakLogic : IUtvonalakLogic
    {
        private readonly IUtvonalakRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UtvonalakLogic"/> class.
        /// </summary>
        /// <param name="repo"></param>
        public UtvonalakLogic(IUtvonalakRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UtvonalakLogic"/> class.
        /// </summary>
        public UtvonalakLogic()
        {
            this.repo = new UtvonalakRepository(new DatabaseEntities());
        }

        /// <summary>
        /// Érkezés módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_erkezes">Új érkezés.</param>
        public void Erkezes_Modositas(int id, DateTime uj_erkezes)
        {
            this.repo.Erkezes_Modositas(id, uj_erkezes);
        }

        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Utvonalak GetOne(int id)
        {
            return this.repo.GetOne(id);
        }

        /// <summary>
        /// Honnan módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_honnan">Honnan.</param>
        public void Honnan_Modositas(int id, string uj_honnan)
        {
            this.repo.Honnan_Modositas(id, uj_honnan);
        }

        /// <summary>
        /// Hova módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_hova">Új hova.</param>
        public void Hova_Modositas(int id, string uj_hova)
        {
            this.repo.Hova_Modositas(id, uj_hova);
        }

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="utvonalak">Új útvonal.</param>
        public void Hozzaadas(Utvonalak utvonalak)
        {
            this.repo.Hozzaadas(utvonalak);
        }

        /// <summary>
        /// Indulás módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_indulas">Új idnulás.</param>
        public void Indula_Modositas(int id, DateTime uj_indulas)
        {
            this.repo.Indulas_Modositas(id, uj_indulas);
        }

        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>Utvonalak lista.</returns>
        public IQueryable<Utvonalak> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// Osszeshely módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_osszesHely">Új összeshely.</param>
        public void OsszesHely_Modositas(int id, int uj_osszesHely)
        {
            this.repo.OsszesHely_Modositas(id, uj_osszesHely);
        }

        /// <summary>
        /// Szabadhely módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szabadHely">Új szabad hely.</param>
        public void SzabadHely_Modositas(int id, int uj_szabadHely)
        {
            this.repo.SzabadHely_Modositas(id, uj_szabadHely);
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            this.repo.Torles(id);
        }
    }
}
