﻿// <copyright file="nonCRUDLogic.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;

    /// <summary>
    /// NonCrud osztály.
    /// </summary>
    public class NonCRUDLogic : InonCRUDLOGIC
    {
        private readonly IHajokRepository hRepo;
        private readonly IFoglalasRepository foRepo;
        private readonly IFelhasznaloRepository feRepo;
        private readonly IUtvonalakRepository uRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="hRepo">Repo.</param>
        public NonCRUDLogic(IHajokRepository hRepo)
        {
            this.hRepo = hRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="foRepo">FoglalasRepo.</param>
        public NonCRUDLogic(IFoglalasRepository foRepo)
        {
            this.foRepo = foRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="feRepo">FelhasznaloRepo.</param>
        public NonCRUDLogic(IFelhasznaloRepository feRepo)
        {
            this.feRepo = feRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="uRepo">UtvonalRepo.</param>
        public NonCRUDLogic(IUtvonalakRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        public NonCRUDLogic()
        {
            DatabaseEntities ctx = new DatabaseEntities();
            this.feRepo = new FelhasznaloRepository(ctx);
            this.foRepo = new FoglalasRepository(ctx);
            this.hRepo = new HajokRepository(ctx);
            this.uRepo = new UtvonalakRepository(ctx);
        }

        /// <summary>
        /// FizetettNemFizetettArany.
        /// </summary>
        /// <returns>FizetettNemFizetettArany lista.</returns>
        public List<FizetettNemFizetettArany> FizetettNemFizetettArany()
        {
            var q = from x in this.foRepo.Listazas()
                    group x by x.UtvonalID into g
                    select new FizetettNemFizetettArany { UtvonalId = (int)g.Key, Arany = g.Average(x => x.FizStat.Value) };
            return q.ToList();
        }

        /// <summary>
        /// Hirlevel.
        /// </summary>
        /// <returns>Hirvelel lista.</returns>
        public List<Hirlevel> Hirlevel()
        {
            var q = from x in this.feRepo.Listazas()
                    select new Hirlevel { Nev = x.Nev, Email = x.Email, Felhasznaloid = (int)x.FelhasznaloID };
            return q.ToList();
        }

        /// <summary>
        /// ToNewYorkHelyek.
        /// </summary>
        /// <returns>ToNewYorkHelyek lista.</returns>
        public List<ToNewYorkHelyek> ToNewYorkHelyek()
        {
            var q = from x in this.uRepo.Listazas()
                    where x.Hova.Equals("New York")
                    select new ToNewYorkHelyek { UtvonalId = (int)x.UtvonalID, Hova = x.Hova, OsszesHely = (int)x.OsszesHely };
            return q.ToList();
        }
    }

    /// <summary>
    /// FizetettNemFizetett osztály.
    /// </summary>
    public class FizetettNemFizetettArany
    {
        /// <summary>
        /// Gets or sets utvonalid.
        /// </summary>
        public int UtvonalId { get; set; }

        /// <summary>
        /// Gets or sets arany.
        /// </summary>
        public double Arany { get; set; }

        /// <summary>
        /// Equals felülírás.
        /// </summary>
        /// <param name="obj">Egy objektum.</param>
        /// <returns>Igaz vagy hamis.</returns>
        public override bool Equals(object obj)
        {
            if (obj is FizetettNemFizetettArany)
            {
                FizetettNemFizetettArany other = obj as FizetettNemFizetettArany;
                return this.UtvonalId == other.UtvonalId;
            }

            return false;
        }

        /// <summary>
        /// HashCode generálás.
        /// </summary>
        /// <returns>Hash kód.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }

    /// <summary>
    /// Hirevel osztaly.
    /// </summary>
    public class Hirlevel
    {
        /// <summary>
        /// Gets or sets standard text.
        /// </summary>
        public int Felhasznaloid { get; set; }

        /// <summary>
        /// Gets or sets név.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Equals.
        /// </summary>
        /// <param name="obj">Objketum.</param>
        /// <returns>Egyezik e.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Hirlevel)
            {
                Hirlevel other = obj as Hirlevel;
                return this.Felhasznaloid == other.Felhasznaloid;
            }

            return false;
        }

        /// <summary>
        /// Hash code.
        /// </summary>
        /// <returns>Kód.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }

    /// <summary>
    /// ToNewYorkHelyek osztal.
    /// </summary>
    public class ToNewYorkHelyek
    {
        /// <summary>
        /// Gets or sets fad.
        /// </summary>
        public int UtvonalId { get; set; }

        /// <summary>
        /// Gets or sets fad.
        /// </summary>
        public int OsszesHely { get; set; }

        /// <summary>
        /// Gets or sets fad.
        /// </summary>
        public string Hova { get; set; }

        /// <summary>
        /// Equals.
        /// </summary>
        /// <param name="obj">Objetkum.</param>
        /// <returns>Ugynazok-e.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ToNewYorkHelyek)
            {
                ToNewYorkHelyek other = obj as ToNewYorkHelyek;
                return this.UtvonalId == other.UtvonalId;
            }

            return false;
        }

        /// <summary>
        /// Hash cofe generálás.
        /// </summary>
        /// <returns>Kód.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
