﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HajosOldal.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:58315/api/FelhasznalokApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed" : "Operation failed";
            Messenger.Default.Send(msg, "FelhResult");
        }

        public List<FelhasznaloVM> ApiGetFelhasznalok()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<FelhasznaloVM>>(json);
            return list;
        }

        public void ApiDelFelh(FelhasznaloVM felh)
        {
            bool success = false;
            if(felh != null)
            {
                string json = client.GetStringAsync(url + "del/" + felh.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }
        bool ApiEditFelh(FelhasznaloVM felh, bool isEditing)
        {
            if (felh == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add(nameof(FelhasznaloVM.Id), felh.Id.ToString());
            postData.Add(nameof(FelhasznaloVM.Nev), felh.Nev.ToString());
            postData.Add(nameof(FelhasznaloVM.Email), felh.Email.ToString());
            postData.Add(nameof(FelhasznaloVM.Telefon), felh.Telefon.ToString());
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditFelhasznalo(FelhasznaloVM felh, Func<FelhasznaloVM, bool> editor)
        {
            FelhasznaloVM clone = new FelhasznaloVM();
            if (felh != null) clone.CopyFrom(felh);
            bool? success = editor?.Invoke(clone);
            if(success == true)
            {
                if (felh != null) ApiEditFelh(clone, true);
                else ApiEditFelh(clone, false);
            }
            SendMessage(success == true);
        }

    }
}
