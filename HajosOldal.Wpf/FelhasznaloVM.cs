﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HajosOldal.Wpf
{
    class FelhasznaloVM : ObservableObject
    {
        private int id;
        private string nev;
        private string email;
        private string telefon;

        public string Telefon
        {
            get { return telefon; }
            set { Set(ref telefon, value); }
        }


        public string Email
        {
            get { return email; }
            set { Set(ref email, value); }
        }


        public string Nev
        {
            get { return nev; }
            set { Set(ref nev, value); }
        }

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }



        public void CopyFrom(FelhasznaloVM other)
        {
            if (other == null) return;
            this.Id = other.id;
            this.Nev = other.nev;
            this.Email = other.nev;
            this.Telefon = other.telefon;
        }
    }
}
