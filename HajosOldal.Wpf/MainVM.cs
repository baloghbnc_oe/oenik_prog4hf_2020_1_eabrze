﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HajosOldal.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private FelhasznaloVM selectedFelh;
        private ObservableCollection<FelhasznaloVM> allFelh;

        public ObservableCollection<FelhasznaloVM> AllFelh
        {
            get { return allFelh; }
            set { Set(ref allFelh, value); }
        }


        public FelhasznaloVM SelectedFelh
        {
            get { return selectedFelh; }
            set { Set(ref selectedFelh, value); }
        }


        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<FelhasznaloVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelFelh(selectedFelh));
            AddCmd = new RelayCommand(() => logic.EditFelhasznalo(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditFelhasznalo(selectedFelh, EditorFunc));
            LoadCmd = new RelayCommand(() => 
            AllFelh = new ObservableCollection<FelhasznaloVM>(logic.ApiGetFelhasznalok()));


        }


    }
}
