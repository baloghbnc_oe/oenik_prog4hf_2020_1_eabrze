﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Felhasznalok.ConsoleClient
{
    public class Felhasznalo
    {
        public int Id { get; set; }
        public string Nev { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return $"Id={Id}\tNev={Nev}\tTelefon{Telefon}\tEmail{Email}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting......");
            Console.ReadKey();

            string url = "http://localhost:58315/api/FelhasznalokApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Felhasznalo>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Felhasznalo.Id), "10");
                postData.Add(nameof(Felhasznalo.Nev), "Kis Béla");
                postData.Add(nameof(Felhasznalo.Email), "e@mail.tld");
                postData.Add(nameof(Felhasznalo.Telefon), "24324");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result
                    .Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);

                Console.ReadKey();

                int felhasznaloId = JsonConvert.DeserializeObject<List<Felhasznalo>>(json).FirstOrDefault(x => x.Nev == "Kis Béla").Id;
                
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Felhasznalo.Nev), "Kis Béla");
                postData.Add(nameof(Felhasznalo.Email), "email@mail.tld");
                postData.Add(nameof(Felhasznalo.Telefon), "24324");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result
                    .Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadKey();


                response = client.GetStringAsync(url + "del/" + 10).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadKey();




            }
        }
    }
}
