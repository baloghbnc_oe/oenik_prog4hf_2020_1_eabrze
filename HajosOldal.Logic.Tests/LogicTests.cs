﻿// <copyright file="LogicTests.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HajosOldal.Data;
    using HajosOldal.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic teszthez létrehozott osztály.
    /// </summary>
    [TestFixture]
    internal class LogicTests
    {
        /// <summary>
        /// Össze útvonal listázása teszt.
        /// </summary>
        [Test]
        public void TestGetAllUtvonal()
        {
            // ARRANGE
            Mock<IUtvonalakRepository> mockedRepo = new Mock<IUtvonalakRepository>();

            List<Utvonalak> utvonalak = new List<Utvonalak>()
            {
                new Utvonalak() { UtvonalID = 1, HajoID = 1, Erkezes = DateTime.Today, Indulas = DateTime.Today, Honnan = "Budapest", Hova = "Bécs", OsszesHely = 5, SzabadHely = 1 },
                new Utvonalak() { UtvonalID = 2, HajoID = 2, Erkezes = DateTime.Today, Indulas = DateTime.Today, Honnan = "Bécs", Hova = "Budapest", OsszesHely = 5, SzabadHely = 2 },
            };
            mockedRepo.Setup(r => r.Listazas()).Returns(utvonalak.AsQueryable());
            List<int> lista = new List<int> { 1, 2 };
            UtvonalakLogic uLogic = new UtvonalakLogic(mockedRepo.Object);

            // ACT
            List<Utvonalak> result = uLogic.Listazas().ToList();

            // ASSERT
            mockedRepo.Verify(r => r.Listazas(), Times.Once);
            Assert.That(result.Select(x => x.UtvonalID), Is.EqualTo(lista));
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.Select(x => x.UtvonalID), Is.Unique);
        }

        /// <summary>
        /// Új hajó hozzáadása teszt.
        /// </summary>
        [Test]
        public void TestHajoHozzaadas()
        {
            // Arrange
            Mock<IHajokRepository> mockedRepo = new Mock<IHajokRepository>();

            List<Hajok> hajok = new List<Hajok>
            {
                new Hajok() { HajoID = 1, Nev = "Tesztitek", Ferohely = 1, Kabinok = 1, Tipus = "teszt", Szemelyzet = 1 },
                new Hajok() { HajoID = 2, Nev = "Tesztecske", Ferohely = 2, Kabinok = 1, Tipus = "tesztis", Szemelyzet = 2 },
            };

            Hajok ujHajo = new Hajok() { HajoID = 3, Nev = "NincsNev", Ferohely = 34, Kabinok = 12, Tipus = "asff", Szemelyzet = 2 };

            mockedRepo.Setup(r => r.Hozzaadas(ujHajo)).Callback<Hajok>((h) =>
            {
                hajok.Add(h);
            });
            HajokLogic hLogic = new HajokLogic(mockedRepo.Object);

            // ACT
            hLogic.Hozzaadas(ujHajo);

            // ASSERT
            mockedRepo.Verify(repo => repo.Hozzaadas(It.IsAny<Hajok>()), Times.Exactly(1));
            Assert.DoesNotThrow(() => hLogic.Hozzaadas(It.IsAny<Hajok>()));
            Assert.That(hajok.Select(x => x.HajoID), Contains.Item(ujHajo.HajoID));
        }

        /// <summary>
        /// Felhasználó nevének módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="nev">Név.</param>
        [TestCase(2, "Teszt Elek Béla")]
        public void TestFelhasznaloNevModositas(int id, string nev)
        {
            // ARRANGE
            Mock<IFelhasznaloRepository> mockedRepo = new Mock<IFelhasznaloRepository>();

            List<Felhasznalo> felhasznalok = new List<Felhasznalo>()
            {
                new Felhasznalo() { FelhasznaloID = 1, Nev = "Mikike az Egér" },
                new Felhasznalo() { FelhasznaloID = 2, Nev = "Tesztike Miksa" },
            };

            List<string> expected = new List<string> { "2", "Teszt Elek Béla" };

            mockedRepo.Setup(r => r.Nev_Modositas(id, nev)).Callback<int, string>((a, b) =>
            felhasznalok.SingleOrDefault(s => s.FelhasznaloID == a).Nev = b);

            FelhasznaloLogic fLogic = new FelhasznaloLogic(mockedRepo.Object);

            // ACT
            fLogic.Nev_Modositas(id, nev);

            // ASSERT
            Assert.That(felhasznalok.Select(x => x.FelhasznaloID), Contains.Item(id));
            mockedRepo.Verify(repo => repo.Nev_Modositas(It.IsAny<int>(), It.IsAny<string>()), Times.Exactly(1));
            Assert.DoesNotThrow(() => fLogic.Nev_Modositas(It.IsAny<int>(), It.IsAny<string>()));
            Assert.That(felhasznalok.Select(x => x.Nev), Contains.Item("Teszt Elek Béla"));
            Assert.That(felhasznalok.Select(x => x.Nev), Does.Not.Contain("Tesztike Miksa"));
        }
        /// <summary>
        /// Felhasználó törlés teszt.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        [TestCase(1)]
        public void TestFelhasznaloTorles(int id)
        {
            // ARRANGE
            Mock<IFelhasznaloRepository> mockedRepo = new Mock<IFelhasznaloRepository>();

            List<Felhasznalo> felhasznalok = new List<Felhasznalo>()
            {
                new Felhasznalo() { FelhasznaloID = 1, Nev = "Teszt Béla" },
                new Felhasznalo() { FelhasznaloID = 2, Nev = "Bent Maradok" },
            };

            mockedRepo.Setup(r => r.Torles(id)).Callback<int>((c) => felhasznalok.RemoveAll(x => x.FelhasznaloID == c));

            FelhasznaloLogic fLogic = new FelhasznaloLogic(mockedRepo.Object);

            // ACT
            fLogic.Torles(id);

            // ASSERT
            mockedRepo.Verify(repo => repo.Torles(It.IsAny<int>()));
            Assert.That(felhasznalok.Select(x => x.FelhasznaloID), Does.Not.Contain(id));
            Assert.That(felhasznalok.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// EgyFoglalás Teszt.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        [TestCase(2)]
        public void TestEgyFoglalas(int id)
        {
            // ARRANGE
            Mock<IFoglalasRepository> mockRepo = new Mock<IFoglalasRepository>();

            List<Foglalas> foglalasok = new List<Foglalas>
             {
                new Foglalas() { FoglalasID = 1, FelhasznaloID = 1 },
                new Foglalas() { FoglalasID = 2, FelhasznaloID = 2 },
                new Foglalas() { FoglalasID = 3, FelhasznaloID = 3 },
             };

            mockRepo.Setup(r => r.GetOne(It.IsAny<int>())).Returns<int>(fid => foglalasok.SingleOrDefault(r => r.FelhasznaloID == fid));
            FoglalasLogic fLogic = new FoglalasLogic(mockRepo.Object);

            // ACT
            var selectedFoglalas = fLogic.GetOne(id);

            // ASSERT
            mockRepo.Verify(repo => repo.GetOne(id), Times.Once);
            Assert.DoesNotThrow(() => fLogic.GetOne(id));
            Assert.IsNotNull(selectedFoglalas);
            Assert.That(id, Is.EqualTo(selectedFoglalas.FelhasznaloID));
            Assert.That(foglalasok, Contains.Item(selectedFoglalas));
        }

        /// <summary>
        /// Hirlevel nonCRUDLogic teszt.
        /// </summary>
        [Test]
        public void TestHirlevel()
        {
            Mock<IFelhasznaloRepository> mockedRepo = new Mock<IFelhasznaloRepository>();

            List<Felhasznalo> felhasznalok = new List<Felhasznalo>
            {
                new Felhasznalo() { FelhasznaloID = 1, Nev = "Teszt Elek", Email = "asd@asd.hu"},
                new Felhasznalo() { FelhasznaloID = 2, Nev = "Miksa Maksa", Email = "dsa@dsa.com" },
                new Felhasznalo() { FelhasznaloID = 3, Nev = "Ki Kelet", Email = "qwe@rt.zu"},
            };

            mockedRepo.Setup(r => r.Listazas()).Returns(felhasznalok.AsQueryable());

            NonCRUDLogic logic = new NonCRUDLogic(mockedRepo.Object);

            var hirlevelesek = logic.Hirlevel();

            mockedRepo.Verify(repo => repo.Listazas(), Times.Once);
            Assert.That(hirlevelesek.Select(y => y.Felhasznaloid), Does.Contain(1));
            Assert.That(hirlevelesek.Select(y => y.Felhasznaloid), Does.Contain(2));
            Assert.That(hirlevelesek.Select(y => y.Felhasznaloid), Does.Contain(3));
        }

        [Test]
        public void TestFizetettNemFizetettArany()
        {
            Mock<IFoglalasRepository> mockedRepo = new Mock<IFoglalasRepository>();

            List<Foglalas> foglalasok = new List<Foglalas>()
            {
                new Foglalas(){FoglalasID = 0, UtvonalID = 1, FizStat = 0 },
                new Foglalas(){FoglalasID = 1, UtvonalID = 1, FizStat = 1 },
                new Foglalas(){FoglalasID = 2, UtvonalID = 2, FizStat = 1 },
                new Foglalas(){FoglalasID = 3, UtvonalID = 2, FizStat = 1 },
            };

            List<FizetettNemFizetettArany> ervenyes = new List<FizetettNemFizetettArany>(){
                new FizetettNemFizetettArany() { UtvonalId = 1, Arany = 0.5},
                new FizetettNemFizetettArany() { UtvonalId = 2, Arany = 1},
            };

            mockedRepo.Setup(r => r.Listazas()).Returns(foglalasok.AsQueryable());

            NonCRUDLogic logic = new NonCRUDLogic(mockedRepo.Object);

            var eredmeny = logic.FizetettNemFizetettArany();

            mockedRepo.Verify(repo => repo.Listazas(), Times.Once);

            Assert.That(eredmeny, Is.EquivalentTo(ervenyes));            
        }

        [Test]
        public void TestToNewYorkHelyek()
        {
            Mock<IUtvonalakRepository> mockedRepo = new Mock<IUtvonalakRepository>();

            List<Utvonalak> utvonalak = new List<Utvonalak>()
            {
                new Utvonalak(){UtvonalID = 1, Hova = "New York", OsszesHely = 123},
                new Utvonalak(){UtvonalID = 1, Hova = "New York", OsszesHely = 342},
                new Utvonalak(){UtvonalID = 2, Hova = "New York", OsszesHely = 21},
            };

            List<ToNewYorkHelyek> ervenyes = new List<ToNewYorkHelyek>(){
                new ToNewYorkHelyek() {UtvonalId = 1, Hova = "New York", OsszesHely = 123 },
                new ToNewYorkHelyek() {UtvonalId = 1, Hova = "New York", OsszesHely = 342 },
                new ToNewYorkHelyek() {UtvonalId = 2, Hova = "New York", OsszesHely = 21 },
            };

            mockedRepo.Setup(r => r.Listazas()).Returns(utvonalak.AsQueryable());

            NonCRUDLogic logic = new NonCRUDLogic(mockedRepo.Object);

            var eredmeny = logic.ToNewYorkHelyek();

            mockedRepo.Verify(repo => repo.Listazas(), Times.Once);

            Assert.That(eredmeny, Is.EquivalentTo(ervenyes));
        }

        

    }
}
