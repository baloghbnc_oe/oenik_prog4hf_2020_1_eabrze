﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HajosOldal.Web.Models
{
    public class FelhasznaloViewModel
    {
        public Felhasznalo EditedFelhasznalo { get; set; }
        public List<Felhasznalo> ListOfFelhasznalok { get; set; }
    }
}