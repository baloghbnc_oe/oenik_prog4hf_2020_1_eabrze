﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HajosOldal.Web.Models
{
    public class Exercise
    {
        public string Name { get; set; }
        public int Calorie { get; set; }
        public Exercise(string name, int calorie)
        {
            Name = name;
            Calorie = calorie;
        }
    }
}