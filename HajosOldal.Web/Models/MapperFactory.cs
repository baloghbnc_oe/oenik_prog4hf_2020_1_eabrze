﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HajosOldal.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<HajosOldal.Data.Felhasznalo, HajosOldal.Web.Models.Felhasznalo>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.FelhasznaloID)).
                ForMember(dest => dest.Nev, map => map.MapFrom(src => src.Nev)).
                ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                ForMember(dest => dest.Telefon, map => map.MapFrom(src => src.Telefon));


            });
            return config.CreateMapper();
        }
    }
}