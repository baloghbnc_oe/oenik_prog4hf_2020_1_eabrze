﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HajosOldal.Web.Models
{
    public class CalorieInput
    {
        public string Nev { get; set; }
        public double Tomeg { get; set; }
        public string Exercise { get; set; }
        public double Length { get; set; }
    }
}