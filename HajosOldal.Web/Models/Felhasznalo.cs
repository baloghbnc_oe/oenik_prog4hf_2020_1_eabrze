﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HajosOldal.Web.Models
{
    public class Felhasznalo
    {
        [Display(Name = "Felhasznalo ID")]
        [Required]
        public int Id { get; set; }
        [Display(Name = "Felhasználó név")]
        [Required]
        public string Nev { get; set; }
        [Display(Name = "Telefonszám")]
        [Required]
        public string Telefon { get; set; }
        [Display(Name = "E-mail cím")]
        [Required]
        public string Email { get; set; }
    }
}