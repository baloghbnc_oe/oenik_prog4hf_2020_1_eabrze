﻿using AutoMapper;
using HajosOldal.Logic;
using HajosOldal.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HajosOldal.Repository;
using System.Data.Entity;

namespace HajosOldal.Web.Controllers
{
    public class FelhasznalokController : Controller
    {
        IFelhasznaloLogic logic;
        IMapper mapper;
        FelhasznaloViewModel vm;
        public FelhasznalokController()
        {
            HajosOldal.Data.DatabaseEntities  ctx = new HajosOldal.Data.DatabaseEntities();
            FelhasznaloRepository felgRepo = new FelhasznaloRepository(ctx);
            logic = new FelhasznaloLogic(felgRepo);
            mapper = MapperFactory.CreateMapper();

            vm = new FelhasznaloViewModel();
            vm.EditedFelhasznalo = new Felhasznalo();
            var felhasznalok = logic.Listazas();
            vm.ListOfFelhasznalok = mapper.Map < IQueryable<Data.Felhasznalo>, List<Models.Felhasznalo>>(felhasznalok);

        }

        private Felhasznalo GetFelhasznaloModel(int id)
        {
            Data.Felhasznalo oneFelasznalo = logic.GetOne(id);
            return mapper.Map<Data.Felhasznalo, Models.Felhasznalo>(oneFelasznalo);
        }
        // GET: Felhasznalok
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("FelhasznaloIndex",vm);
        }

        // GET: Felhasznalok/Details/5
        public ActionResult Detail(int id)
        {
            return View("FelhasznalokAdatok",GetFelhasznaloModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (true)
            {
                logic.Torles(id);
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedFelhasznalo = GetFelhasznaloModel(id);
            return View("FelhasznaloEdit", vm.EditedFelhasznalo);
        }
        [HttpPost]
        public ActionResult Edit(Felhasznalo felhasznalo, string editAction)
        {
            if(ModelState.IsValid && felhasznalo != null)
            {
                TempData["editResult"] = "EditOK";
                if(editAction == "AddNew")
                {
                    Data.Felhasznalo felhUj = new Data.Felhasznalo();
                    felhUj.FelhasznaloID = felhasznalo.Id;
                    felhUj.Nev = felhasznalo.Nev;
                    felhUj.Email = felhasznalo.Email;
                    felhUj.Telefon = felhasznalo.Telefon;

                    logic.Hozzaadas(felhUj);
                }
                else
                {                    
                    logic.Nev_Modositas(felhasznalo.Id, felhasznalo.Nev);
                    logic.Email_Modositas(felhasznalo.Id, felhasznalo.Email);
                    logic.Telefon_Modositas(felhasznalo.Id, felhasznalo.Telefon);
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedFelhasznalo = felhasznalo;
                return View("FelhasznaloIndex", vm);
            }
        }
    }
}
