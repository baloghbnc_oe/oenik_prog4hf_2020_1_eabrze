﻿using AutoMapper;
using HajosOldal.Logic;
using HajosOldal.Repository;
using HajosOldal.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HajosOldal.Web.Controllers
{
    public class FelhasznalokApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IFelhasznaloLogic logic;
        IMapper mapper;
        public FelhasznalokApiController()
        {
            HajosOldal.Data.DatabaseEntities ctx = new HajosOldal.Data.DatabaseEntities();
            FelhasznaloRepository felgRepo = new FelhasznaloRepository(ctx);
            logic = new FelhasznaloLogic(felgRepo);
            mapper = MapperFactory.CreateMapper();
        }

        // GET api/FelhasznalokApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Felhasznalo> GetAll()
        {
            var felhasznalok = logic.Listazas();
            return mapper.Map<IQueryable<Data.Felhasznalo>, List<Models.Felhasznalo>>(felhasznalok);
        }

        //GET api/FelhasznalokApi/del/1
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneFelhasznalo(int id)
        {
            bool succes = true;
            logic.Torles(id);
            return new ApiResult() { OperationResult = succes };
        }

        //POST api/FelhasznalokApi/del/1
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneFelhasznalo(Felhasznalo felhasznalo)
        {
            logic.Hozzaadas(new Data.Felhasznalo() {Nev = felhasznalo.Nev, FelhasznaloID = felhasznalo.Id, Email = felhasznalo.Email, Telefon = felhasznalo.Telefon });
            return new ApiResult() { OperationResult = true };
        }

        //GET api/FelhasznalokApi/mod/1
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneFelhasznalo(Felhasznalo felhasznalo)
        {
            logic.Nev_Modositas(felhasznalo.Id, felhasznalo.Nev);
            logic.Email_Modositas(felhasznalo.Id, felhasznalo.Email);
            logic.Telefon_Modositas(felhasznalo.Id, felhasznalo.Telefon);
            return new ApiResult() { OperationResult = true };
        }
    }
}
