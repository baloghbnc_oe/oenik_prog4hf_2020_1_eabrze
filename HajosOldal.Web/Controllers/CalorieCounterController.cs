﻿using HajosOldal.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HajosOldal.Web.Controllers
{
    public class CalorieCounterController : Controller
    {
        public ActionResult Index()
        {
            return View("CalInput");
        }

        public ActionResult Calorie()
        {
            return View("CalInput");
        }

        public double getCalorie(CalorieInput ci)
        {
            List<Exercise> gyakorlatok = new List<Exercise>();
            gyakorlatok.Add(new Exercise("Running", 1000));
            gyakorlatok.Add(new Exercise("Yoga", 400));
            gyakorlatok.Add(new Exercise("Pilates", 472));
            gyakorlatok.Add(new Exercise("Hiking", 700));
            gyakorlatok.Add(new Exercise("Swimming", 1000));
            gyakorlatok.Add(new Exercise("Bicycle", 600));

            double eredmeny = 0;

            foreach (Exercise elem in gyakorlatok)
            {
                if (elem.Name == ci.Exercise)
                {
                    eredmeny = elem.Calorie * (ci.Tomeg / 100) * (ci.Length / 60);
                }
            }
            return eredmeny;
        }

        [HttpPost]
        public ActionResult Calorie(CalorieInput ci)
        {
            Eredmeny eredmeny = new Eredmeny()
            {
                CI = ci,
                Calorie = getCalorie(ci),
            };
            return View("CalResult", eredmeny);
        }


    }
}