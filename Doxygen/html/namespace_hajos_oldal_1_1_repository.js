var namespace_hajos_oldal_1_1_repository =
[
    [ "FelhasznaloRepository", "class_hajos_oldal_1_1_repository_1_1_felhasznalo_repository.html", "class_hajos_oldal_1_1_repository_1_1_felhasznalo_repository" ],
    [ "FoglalasRepository", "class_hajos_oldal_1_1_repository_1_1_foglalas_repository.html", "class_hajos_oldal_1_1_repository_1_1_foglalas_repository" ],
    [ "HajokRepository", "class_hajos_oldal_1_1_repository_1_1_hajok_repository.html", "class_hajos_oldal_1_1_repository_1_1_hajok_repository" ],
    [ "IFelhasznaloRepository", "interface_hajos_oldal_1_1_repository_1_1_i_felhasznalo_repository.html", "interface_hajos_oldal_1_1_repository_1_1_i_felhasznalo_repository" ],
    [ "IFoglalasRepository", "interface_hajos_oldal_1_1_repository_1_1_i_foglalas_repository.html", "interface_hajos_oldal_1_1_repository_1_1_i_foglalas_repository" ],
    [ "IHajokRepository", "interface_hajos_oldal_1_1_repository_1_1_i_hajok_repository.html", "interface_hajos_oldal_1_1_repository_1_1_i_hajok_repository" ],
    [ "IRepository", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", "interface_hajos_oldal_1_1_repository_1_1_i_repository" ],
    [ "IUtvonalakRepository", "interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository.html", "interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository" ],
    [ "Repository", "class_hajos_oldal_1_1_repository_1_1_repository.html", "class_hajos_oldal_1_1_repository_1_1_repository" ],
    [ "UtvonalakRepository", "class_hajos_oldal_1_1_repository_1_1_utvonalak_repository.html", "class_hajos_oldal_1_1_repository_1_1_utvonalak_repository" ]
];