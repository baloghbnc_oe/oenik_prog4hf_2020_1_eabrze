var hierarchy =
[
    [ "DbContext", null, [
      [ "HajosOldal.Data::DatabaseEntities", "class_hajos_oldal_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "HajosOldal.Data.Felhasznalo", "class_hajos_oldal_1_1_data_1_1_felhasznalo.html", null ],
    [ "HajosOldal.Logic.FizetettNemFizetettArany", "class_hajos_oldal_1_1_logic_1_1_fizetett_nem_fizetett_arany.html", null ],
    [ "HajosOldal.Data.Foglalas", "class_hajos_oldal_1_1_data_1_1_foglalas.html", null ],
    [ "HajosOldal.Data.Hajok", "class_hajos_oldal_1_1_data_1_1_hajok.html", null ],
    [ "HajosOldal.Logic.Hirlevel", "class_hajos_oldal_1_1_logic_1_1_hirlevel.html", null ],
    [ "HajosOldal.Logic.ILogic< T >", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", null ],
    [ "HajosOldal.Logic.ILogic< Felhasznalo >", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", [
      [ "HajosOldal.Logic.IFelhasznaloLogic", "interface_hajos_oldal_1_1_logic_1_1_i_felhasznalo_logic.html", [
        [ "HajosOldal.Logic.FelhasznaloLogic", "class_hajos_oldal_1_1_logic_1_1_felhasznalo_logic.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Logic.ILogic< Foglalas >", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", [
      [ "HajosOldal.Logic.IFoglalasLogic", "interface_hajos_oldal_1_1_logic_1_1_i_foglalas_logic.html", [
        [ "HajosOldal.Logic.FoglalasLogic", "class_hajos_oldal_1_1_logic_1_1_foglalas_logic.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Logic.ILogic< Hajok >", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", [
      [ "HajosOldal.Logic.IHajokLogic", "interface_hajos_oldal_1_1_logic_1_1_i_hajok_logic.html", [
        [ "HajosOldal.Logic.HajokLogic", "class_hajos_oldal_1_1_logic_1_1_hajok_logic.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Logic.ILogic< Utvonalak >", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", [
      [ "HajosOldal.Logic.IUtvonalakLogic", "interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic.html", [
        [ "HajosOldal.Logic.UtvonalakLogic", "class_hajos_oldal_1_1_logic_1_1_utvonalak_logic.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Logic.InonCRUDLOGIC", "interface_hajos_oldal_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html", [
      [ "HajosOldal.Logic.NonCRUDLogic", "class_hajos_oldal_1_1_logic_1_1_non_c_r_u_d_logic.html", null ]
    ] ],
    [ "HajosOldal.Repository.IRepository< T >", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", [
      [ "HajosOldal.Repository.Repository< T >", "class_hajos_oldal_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "HajosOldal.Repository.IRepository< Felhasznalo >", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", [
      [ "HajosOldal.Repository.IFelhasznaloRepository", "interface_hajos_oldal_1_1_repository_1_1_i_felhasznalo_repository.html", [
        [ "HajosOldal.Repository.FelhasznaloRepository", "class_hajos_oldal_1_1_repository_1_1_felhasznalo_repository.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Repository.IRepository< Foglalas >", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", [
      [ "HajosOldal.Repository.IFoglalasRepository", "interface_hajos_oldal_1_1_repository_1_1_i_foglalas_repository.html", [
        [ "HajosOldal.Repository.FoglalasRepository", "class_hajos_oldal_1_1_repository_1_1_foglalas_repository.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Repository.IRepository< Hajok >", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", [
      [ "HajosOldal.Repository.IHajokRepository", "interface_hajos_oldal_1_1_repository_1_1_i_hajok_repository.html", [
        [ "HajosOldal.Repository.HajokRepository", "class_hajos_oldal_1_1_repository_1_1_hajok_repository.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Repository.IRepository< Utvonalak >", "interface_hajos_oldal_1_1_repository_1_1_i_repository.html", [
      [ "HajosOldal.Repository.IUtvonalakRepository", "interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository.html", [
        [ "HajosOldal.Repository.UtvonalakRepository", "class_hajos_oldal_1_1_repository_1_1_utvonalak_repository.html", null ]
      ] ]
    ] ],
    [ "HajosOldal.Repository.Repository< Felhasznalo >", "class_hajos_oldal_1_1_repository_1_1_repository.html", [
      [ "HajosOldal.Repository.FelhasznaloRepository", "class_hajos_oldal_1_1_repository_1_1_felhasznalo_repository.html", null ]
    ] ],
    [ "HajosOldal.Repository.Repository< Foglalas >", "class_hajos_oldal_1_1_repository_1_1_repository.html", [
      [ "HajosOldal.Repository.FoglalasRepository", "class_hajos_oldal_1_1_repository_1_1_foglalas_repository.html", null ]
    ] ],
    [ "HajosOldal.Repository.Repository< Hajok >", "class_hajos_oldal_1_1_repository_1_1_repository.html", [
      [ "HajosOldal.Repository.HajokRepository", "class_hajos_oldal_1_1_repository_1_1_hajok_repository.html", null ]
    ] ],
    [ "HajosOldal.Repository.Repository< Utvonalak >", "class_hajos_oldal_1_1_repository_1_1_repository.html", [
      [ "HajosOldal.Repository.UtvonalakRepository", "class_hajos_oldal_1_1_repository_1_1_utvonalak_repository.html", null ]
    ] ],
    [ "HajosOldal.Logic.ToNewYorkHelyek", "class_hajos_oldal_1_1_logic_1_1_to_new_york_helyek.html", null ],
    [ "HajosOldal.Data.Utvonalak", "class_hajos_oldal_1_1_data_1_1_utvonalak.html", null ]
];