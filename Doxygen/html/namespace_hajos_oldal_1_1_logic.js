var namespace_hajos_oldal_1_1_logic =
[
    [ "FelhasznaloLogic", "class_hajos_oldal_1_1_logic_1_1_felhasznalo_logic.html", "class_hajos_oldal_1_1_logic_1_1_felhasznalo_logic" ],
    [ "FizetettNemFizetettArany", "class_hajos_oldal_1_1_logic_1_1_fizetett_nem_fizetett_arany.html", "class_hajos_oldal_1_1_logic_1_1_fizetett_nem_fizetett_arany" ],
    [ "FoglalasLogic", "class_hajos_oldal_1_1_logic_1_1_foglalas_logic.html", "class_hajos_oldal_1_1_logic_1_1_foglalas_logic" ],
    [ "HajokLogic", "class_hajos_oldal_1_1_logic_1_1_hajok_logic.html", "class_hajos_oldal_1_1_logic_1_1_hajok_logic" ],
    [ "Hirlevel", "class_hajos_oldal_1_1_logic_1_1_hirlevel.html", "class_hajos_oldal_1_1_logic_1_1_hirlevel" ],
    [ "IFelhasznaloLogic", "interface_hajos_oldal_1_1_logic_1_1_i_felhasznalo_logic.html", "interface_hajos_oldal_1_1_logic_1_1_i_felhasznalo_logic" ],
    [ "IFoglalasLogic", "interface_hajos_oldal_1_1_logic_1_1_i_foglalas_logic.html", "interface_hajos_oldal_1_1_logic_1_1_i_foglalas_logic" ],
    [ "IHajokLogic", "interface_hajos_oldal_1_1_logic_1_1_i_hajok_logic.html", "interface_hajos_oldal_1_1_logic_1_1_i_hajok_logic" ],
    [ "ILogic", "interface_hajos_oldal_1_1_logic_1_1_i_logic.html", "interface_hajos_oldal_1_1_logic_1_1_i_logic" ],
    [ "InonCRUDLOGIC", "interface_hajos_oldal_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html", "interface_hajos_oldal_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c" ],
    [ "IUtvonalakLogic", "interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic.html", "interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic" ],
    [ "NonCRUDLogic", "class_hajos_oldal_1_1_logic_1_1_non_c_r_u_d_logic.html", "class_hajos_oldal_1_1_logic_1_1_non_c_r_u_d_logic" ],
    [ "ToNewYorkHelyek", "class_hajos_oldal_1_1_logic_1_1_to_new_york_helyek.html", "class_hajos_oldal_1_1_logic_1_1_to_new_york_helyek" ],
    [ "UtvonalakLogic", "class_hajos_oldal_1_1_logic_1_1_utvonalak_logic.html", "class_hajos_oldal_1_1_logic_1_1_utvonalak_logic" ]
];