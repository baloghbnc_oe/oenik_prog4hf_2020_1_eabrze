var searchData=
[
  ['ifelhasznalologic_34',['IFelhasznaloLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_felhasznalo_logic.html',1,'HajosOldal::Logic']]],
  ['ifelhasznalorepository_35',['IFelhasznaloRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_felhasznalo_repository.html',1,'HajosOldal::Repository']]],
  ['ifoglalaslogic_36',['IFoglalasLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_foglalas_logic.html',1,'HajosOldal::Logic']]],
  ['ifoglalasrepository_37',['IFoglalasRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_foglalas_repository.html',1,'HajosOldal::Repository']]],
  ['ihajoklogic_38',['IHajokLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_hajok_logic.html',1,'HajosOldal::Logic']]],
  ['ihajokrepository_39',['IHajokRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_hajok_repository.html',1,'HajosOldal::Repository']]],
  ['ilogic_40',['ILogic',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20felhasznalo_20_3e_41',['ILogic&lt; Felhasznalo &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20foglalas_20_3e_42',['ILogic&lt; Foglalas &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20hajok_20_3e_43',['ILogic&lt; Hajok &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20utvonalak_20_3e_44',['ILogic&lt; Utvonalak &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['indula_5fmodositas_45',['Indula_Modositas',['../interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic.html#a6c133ac966568c424cde06acaa7956d6',1,'HajosOldal.Logic.IUtvonalakLogic.Indula_Modositas()'],['../class_hajos_oldal_1_1_logic_1_1_utvonalak_logic.html#a32466b1b4370138df0f340e0c3b79f36',1,'HajosOldal.Logic.UtvonalakLogic.Indula_Modositas()']]],
  ['indulas_5fmodositas_46',['Indulas_Modositas',['../interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository.html#adc63c3972a1e5a5f2e4b79c24374d726',1,'HajosOldal.Repository.IUtvonalakRepository.Indulas_Modositas()'],['../class_hajos_oldal_1_1_repository_1_1_utvonalak_repository.html#af40b0b8e94665cd16d66ff8063b9b3c2',1,'HajosOldal.Repository.UtvonalakRepository.Indulas_Modositas()']]],
  ['inoncrudlogic_47',['InonCRUDLOGIC',['../interface_hajos_oldal_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html',1,'HajosOldal::Logic']]],
  ['irepository_48',['IRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20felhasznalo_20_3e_49',['IRepository&lt; Felhasznalo &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20foglalas_20_3e_50',['IRepository&lt; Foglalas &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20hajok_20_3e_51',['IRepository&lt; Hajok &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20utvonalak_20_3e_52',['IRepository&lt; Utvonalak &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['iutvonalaklogic_53',['IUtvonalakLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic.html',1,'HajosOldal::Logic']]],
  ['iutvonalakrepository_54',['IUtvonalakRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository.html',1,'HajosOldal::Repository']]]
];
