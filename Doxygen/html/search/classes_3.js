var searchData=
[
  ['ifelhasznalologic_91',['IFelhasznaloLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_felhasznalo_logic.html',1,'HajosOldal::Logic']]],
  ['ifelhasznalorepository_92',['IFelhasznaloRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_felhasznalo_repository.html',1,'HajosOldal::Repository']]],
  ['ifoglalaslogic_93',['IFoglalasLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_foglalas_logic.html',1,'HajosOldal::Logic']]],
  ['ifoglalasrepository_94',['IFoglalasRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_foglalas_repository.html',1,'HajosOldal::Repository']]],
  ['ihajoklogic_95',['IHajokLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_hajok_logic.html',1,'HajosOldal::Logic']]],
  ['ihajokrepository_96',['IHajokRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_hajok_repository.html',1,'HajosOldal::Repository']]],
  ['ilogic_97',['ILogic',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20felhasznalo_20_3e_98',['ILogic&lt; Felhasznalo &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20foglalas_20_3e_99',['ILogic&lt; Foglalas &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20hajok_20_3e_100',['ILogic&lt; Hajok &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['ilogic_3c_20utvonalak_20_3e_101',['ILogic&lt; Utvonalak &gt;',['../interface_hajos_oldal_1_1_logic_1_1_i_logic.html',1,'HajosOldal::Logic']]],
  ['inoncrudlogic_102',['InonCRUDLOGIC',['../interface_hajos_oldal_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html',1,'HajosOldal::Logic']]],
  ['irepository_103',['IRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20felhasznalo_20_3e_104',['IRepository&lt; Felhasznalo &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20foglalas_20_3e_105',['IRepository&lt; Foglalas &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20hajok_20_3e_106',['IRepository&lt; Hajok &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['irepository_3c_20utvonalak_20_3e_107',['IRepository&lt; Utvonalak &gt;',['../interface_hajos_oldal_1_1_repository_1_1_i_repository.html',1,'HajosOldal::Repository']]],
  ['iutvonalaklogic_108',['IUtvonalakLogic',['../interface_hajos_oldal_1_1_logic_1_1_i_utvonalak_logic.html',1,'HajosOldal::Logic']]],
  ['iutvonalakrepository_109',['IUtvonalakRepository',['../interface_hajos_oldal_1_1_repository_1_1_i_utvonalak_repository.html',1,'HajosOldal::Repository']]]
];
