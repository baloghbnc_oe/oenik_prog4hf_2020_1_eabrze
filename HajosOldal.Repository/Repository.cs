﻿// <copyright file="Repository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Alap repo.
    /// </summary>
    /// <typeparam name="T">Valmilyen entity.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DBContext változó.
        /// </summary>
        private DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="ctx">DbContext változó.</param>
        public Repository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets dB context.
        /// </summary>
        protected DbContext Ctx { get => this.ctx; set => this.ctx = value; }

        /// <summary>
        /// Listázás művelet.
        /// </summary>
        /// <returns>T típusú lista.</returns>
        public IQueryable<T> Listazas()
        {
            return this.Ctx.Set<T>();
        }
    }
}
