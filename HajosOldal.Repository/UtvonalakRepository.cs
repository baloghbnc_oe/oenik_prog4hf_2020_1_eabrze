﻿// <copyright file="UtvonalakRepository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Útvonalak Repo.
    /// </summary>
    public class UtvonalakRepository : Repository<Utvonalak>, IUtvonalakRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UtvonalakRepository"/> class.
        /// </summary>
        /// <param name="ctx">Context.</param>
        public UtvonalakRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Érkezés módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_erkezes">Új érkezés.</param>
        public void Erkezes_Modositas(int id, DateTime uj_erkezes)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.Erkezes = uj_erkezes;
        }

        /// <summary>
        /// Honnan módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_honnan">Új honnan.</param>
        public void Honnan_Modositas(int id, string uj_honnan)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.Honnan = uj_honnan;
        }

        /// <summary>
        /// Hova módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_hova">Új hova.</param>
        public void Hova_Modositas(int id, string uj_hova)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.Hova = uj_hova;
        }

        /// <summary>
        /// Indulás módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_indulas">Új indulás.</param>
        public void Indulas_Modositas(int id, DateTime uj_indulas)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.Indulas = uj_indulas;
        }

        /// <summary>
        /// Öszes Hely módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_osszesHely">Új összes hely.</param>
        public void OsszesHely_Modositas(int id, int uj_osszesHely)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.OsszesHely = uj_osszesHely;
        }

        /// <summary>
        /// Szabad helyek módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szabadHely">Új szabad hely.</param>
        public void SzabadHely_Modositas(int id, int uj_szabadHely)
        {
            var utvonalak = this.GetOne(id);
            utvonalak.SzabadHely = uj_szabadHely;
        }

        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        public Utvonalak GetOne(int id)
        {
            return this.Listazas().SingleOrDefault(x => x.UtvonalID == id);
        }

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="utvonalak">Új útvonal.</param>
        public void Hozzaadas(Utvonalak utvonalak)
        {
            (this.Ctx as DatabaseEntities).Utvonalak.Add(utvonalak);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            Utvonalak utvonalak = this.GetOne(id);
            (this.Ctx as DatabaseEntities).Utvonalak.Remove(utvonalak);
            this.Ctx.SaveChanges();
        }
    }
}
