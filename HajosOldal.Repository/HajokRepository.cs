﻿// <copyright file="HajokRepository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Hajok repository.
    /// </summary>
    public class HajokRepository : Repository<Hajok>, IHajokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HajokRepository"/> class.
        /// Hajok repository. Adatbázis létrehozás.
        /// </summary>
        /// <param name="ctx">Adatbázis.</param>
        public HajokRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(int id, string uj_nev)
        {
            var hajok = this.GetOne(id);
            hajok.Nev = uj_nev;
        }

        /// <summary>
        /// Személyzet módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szemelyzet">Új személy.</param>
        public void Szemelyzet_Modositas(int id, int uj_szemelyzet)
        {
            var hajok = this.GetOne(id);
            hajok.Szemelyzet = uj_szemelyzet;
        }

        /// <summary>
        /// Típus módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_tipus">Új típus.</param>
        public void Tipus_Modositas(int id, string uj_tipus)
        {
            var hajok = this.GetOne(id);
            hajok.Tipus = uj_tipus;
        }

        /// <summary>
        /// Egy lekérése.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy hajó.</returns>
        public Hajok GetOne(int id)
        {
            return this.Listazas().SingleOrDefault(x => x.HajoID == id);
        }

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="hajok">Egy hajó.</param>
        public void Hozzaadas(Hajok hajok)
        {
            (this.Ctx as DatabaseEntities).Hajok.Add(hajok);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            Hajok hajok = this.GetOne(id);
            (this.Ctx as DatabaseEntities).Hajok.Remove(hajok);
            this.Ctx.SaveChanges();
        }
    }
}
