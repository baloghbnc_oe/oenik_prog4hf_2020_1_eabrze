﻿// <copyright file="FoglalasRepository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Foglalás repository.
    /// </summary>
    public class FoglalasRepository : Repository<Foglalas>, IFoglalasRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FoglalasRepository"/> class.
        /// Foglalás repository, adatbázis példány ltrehozás.
        /// </summary>
        /// <param name="ctx">Adatbázis.</param>
        public FoglalasRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Felhasználó modosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_felhasznaloid">Új egyedi azonosító.</param>
        public void Felhasznalo_Modositas(int id, int uj_felhasznaloid)
        {
            var foglalas = this.GetOne(id);
            foglalas.FelhasznaloID = uj_felhasznaloid;
        }

        /// <summary>
        /// Fizetési státusz módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_stat">Új státusz.</param>
        public void FizStat_Modositas(int id, short uj_stat)
        {
            var foglalas = this.GetOne(id);
            foglalas.FizStat = uj_stat;
        }

        /// <inheritdoc/>
        public Foglalas GetOne(int id)
        {
            return this.Listazas().SingleOrDefault(x => x.FoglalasID == id);
        }

        /// <summary>
        /// Új foglalás hozzáadás.
        /// </summary>
        /// <param name="foglalas">Új foglalás.</param>
        public void Hozzaadas(Foglalas foglalas)
        {
            (this.Ctx as DatabaseEntities).Foglalas.Add(foglalas);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            Foglalas foglalas = this.GetOne(id);
            (this.Ctx as DatabaseEntities).Foglalas.Remove(foglalas);
            this.Ctx.SaveChanges();
        }
    }
}
