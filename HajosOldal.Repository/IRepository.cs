﻿// <copyright file="IRepository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Irepository interface.
    /// </summary>
    /// <typeparam name="T">Valamilyen típus..</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Listázás.
        /// </summary>
        /// <returns>T típusó IQuaryable.</returns>
        IQueryable<T> Listazas();
    }

    /// <summary>
    /// IFelhasználóRepo interface.
    /// </summary>
    public interface IFelhasznaloRepository : IRepository<Felhasznalo>
    {
        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy példány.</returns>
        Felhasznalo GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="felhasznalo">Egy adott felhasználó.</param>
        void Hozzaadas(Felhasznalo felhasznalo);

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(int id, string uj_nev);

        /// <summary>
        /// Jelszó módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_jelszo">Új jelszó.</param>
        void Jelszo_Modositas(int id, string uj_jelszo);

        /// <summary>
        /// Email módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_email">Új email.</param>
        void Email_Modositas(int id, string uj_email);

        /// <summary>
        /// Telefon módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        void Telefon_Modositas(int id, string uj_telefon);

        /// <summary>
        /// Személyi okmány száma.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szam">Új szám.</param>
        void SzoSzama_Modositas(int id, string uj_szam);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IFoglalásRepo interface.
    /// </summary>
    public interface IFoglalasRepository : IRepository<Foglalas>
    {
        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        Foglalas GetOne(int id);

        /// <summary>
        /// Hozzáadás.
        /// </summary>
        /// <param name="foglalas">Új foglalás.</param>
        void Hozzaadas(Foglalas foglalas);

        /// <summary>
        /// Felhasználó módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_felhasznaloid">Új felhasználó id.</param>
        void Felhasznalo_Modositas(int id, int uj_felhasznaloid);

        /// <summary>
        /// Fzetési állapot változtatás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_stat">Új fizetési állapot.</param>
        void FizStat_Modositas(int id, short uj_stat);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IHajokRepo interface.
    /// </summary>
    public interface IHajokRepository : IRepository<Hajok>
    {
        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        Hajok GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="hajok">Új hajó.</param>
        void Hozzaadas(Hajok hajok);

        /// <summary>
        /// Név módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(int id, string uj_nev);

        /// <summary>
        /// Típus módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_tipus">Új típus.</param>
        void Tipus_Modositas(int id, string uj_tipus);

        /// <summary>
        /// Személyzet módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szemelyzet">Új személyzet szám.</param>
        void Szemelyzet_Modositas(int id, int uj_szemelyzet);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }

    /// <summary>
    /// IUtvonalakRepo interface.
    /// </summary>
    public interface IUtvonalakRepository : IRepository<Utvonalak>
    {
        /// <summary>
        /// Egy példány.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <returns>Egy adott példány.</returns>
        Utvonalak GetOne(int id);

        /// <summary>
        /// Új hozzáadás.
        /// </summary>
        /// <param name="utvonalak">Új útvonal.</param>
        void Hozzaadas(Utvonalak utvonalak);

        /// <summary>
        /// Szabad helyek módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szabadHely">Új szabadhelyek száma.</param>
        void SzabadHely_Modositas(int id, int uj_szabadHely);

        /// <summary>
        /// Összes hely módosítása.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_osszesHely">Új összes hely.</param>
        void OsszesHely_Modositas(int id, int uj_osszesHely);

        /// <summary>
        /// Indulás módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_indulas">Új indulás.</param>
        void Indulas_Modositas(int id, DateTime uj_indulas);

        /// <summary>
        /// Érzekés módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_erkezes">Új érkezés.</param>
        void Erkezes_Modositas(int id, DateTime uj_erkezes);

        /// <summary>
        /// Honnan módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_honnan">Új honnan.</param>
        void Honnan_Modositas(int id, string uj_honnan);

        /// <summary>
        /// Hova módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_hova">Új hova.</param>
        void Hova_Modositas(int id, string uj_hova);

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        void Torles(int id);
    }
}
