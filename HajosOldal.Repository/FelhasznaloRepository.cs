﻿// <copyright file="FelhasznaloRepository.cs" company="Balogh Bence EABRZE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace HajosOldal.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using HajosOldal.Data;

    /// <summary>
    /// Felhasznalo Repository.
    /// </summary>
    public class FelhasznaloRepository : Repository<Felhasznalo>, IFelhasznaloRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FelhasznaloRepository"/> class.
        /// Felhasznalo Repository ami létrehozza az adatbázis példányát.
        /// </summary>
        /// <param name="ctx">Adatbázis.</param>
        public FelhasznaloRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Email módosítás implementálása.
        /// </summary>
        /// <param name="id">Felhasznló egyedi azonosítója.</param>
        /// <param name="uj_email">Új email cím.</param>
        public void Email_Modositas(int id, string uj_email)
        {
            var felhasznalo = this.GetOne(id);
            felhasznalo.Email = uj_email;
        }

        /// <summary>
        /// Egy felhasználó lekérése.
        /// </summary>
        /// <param name="id">Felhasználó neve.</param>
        /// <returns>Egy felhasználót ad vissza.</returns>
        public Felhasznalo GetOne(int id)
        {
            return this.Listazas().SingleOrDefault(x => x.FelhasznaloID == id);
        }

        /// <summary>
        /// Új felhasználó hozzáadás.
        /// </summary>
        /// <param name="felhasznalo">Új felhasználó objektum.</param>
        public void Hozzaadas(Felhasznalo felhasznalo)
        {
            (this.Ctx as DatabaseEntities).Felhasznalo.Add(felhasznalo);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Jelszó módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_jelszo">Új jelszó.</param>
        public void Jelszo_Modositas(int id, string uj_jelszo)
        {
            var felhasznalo = this.GetOne(id);
            felhasznalo.Jelszo = uj_jelszo;
        }

        /// <summary>
        /// Néb módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(int id, string uj_nev)
        {
            var felhasznalo = this.GetOne(id);
            felhasznalo.Nev = uj_nev;
        }

        /// <summary>
        /// Személyi okmány módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_szam">Új szám.</param>
        public void SzoSzama_Modositas(int id, string uj_szam)
        {
            var felhasznalo = this.GetOne(id);
            felhasznalo.SzoSzama = uj_szam;
        }

        /// <summary>
        /// Telefon módosítás.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        public void Telefon_Modositas(int id, string uj_telefon)
        {
            var felhasznalo = this.GetOne(id);
            felhasznalo.Telefon = uj_telefon;
        }

        /// <summary>
        /// Törlés.
        /// </summary>
        /// <param name="id">Egyedi azonosító.</param>
        public void Torles(int id)
        {
            Felhasznalo felhasznalo = this.GetOne(id);
            (this.Ctx as DatabaseEntities).Felhasznalo.Remove(felhasznalo);
            this.Ctx.SaveChanges();
        }
    }
}
